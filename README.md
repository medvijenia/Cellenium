# Cellenium

Projects name came from Selenium that is integrated with Excel Cells, thus 
makes a very convenient way to test and keep the data visual and easy to maintain.

### _setup_

1. Fill config reader path:
   * copy config.ini path 
   * from core/components/functional/methods navigate to read_config function 
     paste the path to the path variable.
   
2. Fill the config.ini file and start to write your tests.

3. Fill the elements in the page_base.xlsx

4. navigate to core/environment and run _main_ function to create virtual environment 

5. You're ready to go.

