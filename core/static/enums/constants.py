from enum import Enum


class Rest(Enum):
    GET = 1
    POST = 2
